function formCheck()
{
	var name = document.myForm.name;
	var email = document.myForm.email;
	var phone = document.myForm.phone;
	var reasonOther = document.myForm.reasons;
	var description = document.myForm.briefDescription;
	var bestDays = document.myForm.myChoices;

	if (name.value == "")
	{
		alert("Please enter your name!");
		name.focus();
		return false;
	}

	if (email.value =="" && phone.value =="")
	{
		alert("Please enter either an email or a phone number to contact you!");
		email.focus();
		phone.focus();
		return false;
	}

	if (reasonOther.selectedIndex == 3 && description.value =="")
	{
		alert("You've selected 'Other', but did not add a description! Please add one!");
		description.focus();
		return false;
	}

	if (bestDays.checked == false)
	{
		alert("Please check at least one day that is best to contact you!");
		bestDays.focus();
		return false;
	}

	return true;
}